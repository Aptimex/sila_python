#!/usr/bin/python3

import serial
import time
from answerreading import *

#test to recreate the complex commands

"""
ser = serial.Serial(
	port="/dev/ttyUSB0",
	baudrate=9600,
	bytesyze=EIGHTBITS,
	parity=PARITY_NONE,
	stopbits=STOPBITS_ONE,
	timeout=1)
"""
ser = serial.Serial("/dev/ttyUSB0", 9600, timeout=1)

slot = "001"
def Stock_in(slot) :
	bs_reader()
	if bs_reader.Transfert == 0 :
		print("No plate to take in")
	else :
		stockfile = open("Stockage.txt", "r")
		stock = stockfile.read()
		stock = stock.split(";")
		stockfile.close()
		i=0
		while i < len(stock) :
			if stock[i][:3]==slot :
				print(i)
				if stock[i][4] == "1":
					print("Slot taken, choose another one.")
					return()
				else :
					break
			else :
				i+=1
		ser.write("se:cs 001 015\r".encode())
		ser.write("ll:in\r".encode())#init
		bs_reader()
		ser.write("ll:gp 002\r".encode())#open
		bs_reader()
		ser.write("ll:dp 000\r".encode())#turn
		bs_reader()
		ser.write("ll:sp 002\r".encode())#extend
		bs_reader()
		ser.write("ll:h+ 000\r".encode())#raise
		bs_reader()
		ser.write("ll:sp 001\r".encode())#retract
		bs_reader()
		ser.write("ll:wp\r".encode())#turn back
		bs_reader()
		ser.write("ll:gp 001\r".encode())#close
		bs_reader()
		commande="ll:dp "+slot+"\r"
		ser.write(commande.encode())
		bs_reader()
		commande="ll:h+ "+slot+"\r"
		ser.write(commande.encode())#move to place
		bs_reader()
		ser.write("ll:sp 002\r".encode())#extend
		bs_reader()
		commande="ll:h- "+slot+"\r"
		ser.write(commande.encode())#raise
		bs_reader()
		ser.write("ll:sp 001\r".encode())#retract
		bs_reader()
		ser.write("ll:wp\r".encode())#retract
		bs_reader()
		results = ser.readline()
		print(results)
		stockfile = open("Stockage.txt", "r") # stock the state of the stacker in a extern file
		stock = stockfile.read()
		stock = stock.replace(slot+":0",slot+":1")
		stockfile.close()
		stockfile = open("Stockage.txt", "w")
		stockfile.write(stock)
		stockfile.close()

def Stock_out(slot) :
	bs_reader()
	if bs_reader.Transfert == 1 :
		print("Transfert station occupied. Clear area before continuing")
	else :
		stockfile = open("Stockage.txt", "r")
		stock = stockfile.read()
		stock = stock.split(";")
		stockfile.close()
		i=0
		while i < len(stock) :
			if stock[i][:3]==slot :
				if stock[i][4] == "0":
					print("Slot empty, nothing to take out.")
					return()
				else :
					break
			else :
				i+=1
		ser.write("se:cs 001 015\r".encode())
		ser.write("ll:in\r".encode())#init
		bs_reader()
		ser.write("ll:gp 002\r".encode())#turn
		bs_reader()
		commande = "ll:dp "+slot+"\r"
		ser.write(commande.encode())#turn
		bs_reader()
		commande = "ll:h- "+slot+"\r"
		ser.write(commande.encode())#turn
		bs_reader()
		ser.write("ll:sp 002\r".encode())#extend
		bs_reader()
		ser.write("ll:h+ 001\r".encode())#turn
		bs_reader()
		commande = "ll:h+ "+slot+"\r"
		ser.write(commande.encode())#turn
		bs_reader()
		ser.write("ll:sp 001\r".encode())#extend
		bs_reader()
		ser.write("ll:wp\r".encode())#turn
		bs_reader()
		ser.write("ll:dp 000\r".encode())#turn
		bs_reader()
		ser.write("ll:h+ 000\r".encode())#turn
		bs_reader()
		ser.write("ll:sp 002\r".encode())#extend
		bs_reader()
		ser.write("ll:h- 000\r".encode())#extend
		bs_reader()
		ser.write("ll:sp 001\r".encode())#extend
		bs_reader()
		ser.write("ll:wp\r".encode())#wait position
		bs_reader()
		ser.write("ll:gp 001\r".encode())#turn
		bs_reader()
		stockfile = open("Stockage.txt", "r")
		stock = stockfile.read()
		stock = stock.replace(slot+":1",slot+":0")
		stockfile.close()
		stockfile = open("Stockage.txt", "w")
		stockfile.write(stock)
		stockfile.close()

