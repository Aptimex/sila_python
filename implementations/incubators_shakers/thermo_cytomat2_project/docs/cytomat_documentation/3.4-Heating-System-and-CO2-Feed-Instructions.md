**Heating-System- and CO2-Feed-Instructions**

*  The [Heating-System- and CO2-Feed-Instructions](3.4 Heating-System- and CO2-Feed-Instructions) enable the communication and control with the Heating-System and the CO2-Feed.

| Command | Response | Meaning | Test |
| ------ | ------ | ------ | ------ |
| `ch:it` | `tb XX.X YY.Y`  | Query actual and target value of the temperature<br>**Conditions:** none | 02.04.2019 TL: working. But actual and target value are always 99,9. Connection to Heating-Sysem  may be disrupted or not installed |
| `ch:ic` | `cb XX.X YY.Y` | Query actual and target value of the CO2-Feed<br>**Conditions:** none | 02.04.2019 TL: working. But actual and target value are always 99,9. Connection to CO2-Feed  may be disrupted or not installed |
| `ll:it XX.X` | `ok **` or `er**` | Type in target value of temperature<br>**Conditions:** Value must be in the range of the device-control | 02.04.2019 TL: Command is accepted, but target and actual value are not changing. |
| `ll:ic XX.X` | `ok**` or `er**` | Type in target value of CO2-Feed<br>**Conditions:** Value must be in the range of the device-control |  02.04.2019 TL: Command is accepted, but target and actual value are not changing. |

**Example**

![image](uploads/ecb6d8b545b419fa9ec39bd3acdcc158/image.png)

Telegram from [PS](1. Terms) to [PSS](1. Terms): `ch:it`

Response from [PSS](1. Terms) to [PS](1. Terms): `tb 24.0 22.3`

*  The target value of the temperature is 24.0°C and the actual temperature is 22.3°C