**Warning-Register**

*  The [Warning-Register](2.3 Warning-Register) is not empty, if the Warning-Bit in the [Overview-Register](2.1 Overview Register) is set
*  If [Error-Handling](1. Terms) is activated, the [PSS](1. Terms) will try to solve the warning automatically
*  If [Error-Handling](1. Terms) is deactivated or warning wasn't solved, the warning will be written to the [Error-Register](2.4 Error register)

| Value | Meaning |
| ------ | ------ |
| 0x01 | Communication to motor control is disrupted |
| 0x02 | [Micro-Test-Plate](1. Terms) was not mounted on the [Handler](1. Terms) / [Shovel](1. Terms) |
| 0x03 | [Micro-Test-Plate](1. Terms) was not dropped from the [Handler](1. Terms) / [Shovel](1. Terms) |
| 0x04 | [Shovel](1. Terms) was not extended | driving-error of [Handler](1. Terms) | 
| 0x05 | Time-out in procedure |
| 0x06 | [Automatic-Lift-Door](1. Terms) was not opened |
| 0x07 | [Automatic-Lift-Door](1. Terms) was not closed |
| 0x08 | [Shovel](1. Terms) was not retracted |
| 0x09 | Initialising due to opened [Device-Door](1. Terms) |
| 0x0C | [Transfer-Station](1.Terms) not rotated |


**Command**

Telegram from [PS](1. Terms) to [PSS](1. Terms): `ch:bw`

Response from [PSS](1. Terms) to [PS](1. Terms): `bw**`

**Example**

![image](uploads/4f0a161cd155e95fdf54d7ed7d62cfc6/image.png)

*  [Automatic-Lift-Door](1. Terms) is not closed
*  If [Error-Handling](1. Terms) is activated, the [PSS](1. Terms) tries to close the [Automatic-Lift-Door](1. Terms) and delete the [Warning-Register](2.3 Warning-Register)
*  if [Error-Handling](1. Terms) is deactivated or the [Error-Handling](1. Terms) wasn't able to close the [Automatic-Lift-Door](1. Terms), the [PSS](1. Terms) sets the Error-Bit in the [Overview-Register](2.1 Overview Register) and the [Error-Register](2.4. Error-Register) will be written
