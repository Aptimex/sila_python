# ObservableCommand

This folder contains an example project with an observable command.
Using the SiLACodeGenerator the FDL file can be converted into a working example of a server and client implementation.

## Final: ObservableCommand

The sub-directory `final` contains one of these generated implementations with a few manual modifications:

 * The observable command has been implemented in the simulation mode implementation. It accepts a start value and an end value and will increase an internal counter in a given runtime (compare the constructor of the `_simulation` file) (linear interpolation, nothing spectacular).
 * The `_Intermediate` function of the server will return the current value.
 * The `Value` property will do the same. This is just for completeness.
 * The `_Info` method will give an update on the state, including relative progress and remaining time.
 * The client reads a timeout from the `lifeTimeOfExecution` return when the command is first started. If this is not given, a default timeout will be used. During the runtime of the client it will request the current command state and output the intermediate value as well as the properties value (these should be almost equal) . 
 * Once the command finished successfully (ony if timeout > runtime defined in server) the `_Result` call will return the number of calls to the `_Info` and `_Intermediate` function.
 * The client does not request the `Get_Value` property.
 
## Notes on gRPC Streams and Observable Properties

The way streams are implemented in gRPC makes the server cache an undefined number of values, i.e. a client that pauses (e.g. `time.sleep(1.0)`) during reading the values will **always** receive old values. It should thus be ensured that values are read as fast as possible. 
I personally would also recommend to implement a small delay on the server side to slow down the generation of values, so that the client has a realistic chance to keep up.

See also the ObservableProperty example for this.