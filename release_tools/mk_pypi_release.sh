#!/bin/bash

# make pypi release

package_name="my-package"

# installing pypi release tools
# python3 -m pip install --upgrade setuptools wheel twine

# packing everything

python3 setup.py sdist bdist_wheel

# test upload to test.

python3 -m twine upload --repository-url https://test.pypi.org/legacy/ dist/*

echo to test the installation, type:
echo python3 -m pip install --index-url https://test.pypi.org/simple/ --no-deps $package_name

echo check correct installation by import $package_name

echo when everything is checked, do final upload with:

echo twine upload dist/\*

echo done ...